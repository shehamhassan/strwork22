import os
from dotenv import load_dotenv

import streamlit as st
from streamlit_chat import message
import os
from langchain.chains import ConversationChain
from langchain.llms import OpenAI

load_dotenv()
OPENAPI_API_KEY = os.environ.get("OPENAPI_API_KEY")


def load_chain():
    """Logic for loading the chain you want to use should go here."""


    # If an API key has been provided, create an OpenAI language model instance
    if OPENAPI_API_KEY:
        llm = OpenAI(temperature=0.7, openai_api_key=OPENAPI_API_KEY)
    else:
        # If an API key hasn't been provided, display a warning message
        st.warning("Enter your OPENAI API-KEY. Get your OpenAI API key from [here](https://platform.openai.com/account/api-keys).\n")


    chain = ConversationChain(llm=llm)
    return chain

chain = load_chain()

# From here down is all the StreamLit UI.
st.set_page_config(page_title="LangChain Demo", page_icon=":robot:")
st.header("LangChain Demo")

if "generated" not in st.session_state:
    st.session_state["generated"] = []

if "past" not in st.session_state:
    st.session_state["past"] = []


def get_text():
    input_text = st.text_input("You: ", "Hello, how are you?", key="input")
    return input_text


user_input = get_text()

if user_input:
    output = chain.run(input=user_input)

    st.session_state.past.append(user_input)
    st.session_state.generated.append(output)

if st.session_state["generated"]:

    for i in range(len(st.session_state["generated"]) - 1, -1, -1):
        message(st.session_state["generated"][i], key=str(i))
        message(st.session_state["past"][i], is_user=True, key=str(i) + "_user")


# run streamilt app from CLI
# streamlit run streamlit_template.py