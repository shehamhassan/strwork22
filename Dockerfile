FROM python:3.10

RUN apt-get update \
    && apt-get install -y build-essential awscli \
    && mkdir streamlitdeploy

WORKDIR /streamlitdeploy/

COPY requirements.txt ./
COPY .env ./

# COPY code
COPY ./app ./app

RUN pip install --no-cache-dir -r requirements.txt

ENV PYTHONPATH="/streamlitdeploy"
HEALTHCHECK CMD curl --fail http://localhost:8080/_stcore/health
ENTRYPOINT ["/bin/bash", "-c", "streamlit run app/streamlit_main.py --server.port=8080 --server.address=0.0.0.0"]

EXPOSE 8080
